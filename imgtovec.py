import csv
import os
import re
from typing import Optional

import PIL
import numpy as np
import pandas as pd
from PIL import Image
from matplotlib import pyplot as plt
from sentence_transformers import SentenceTransformer
from sklearn.decomposition import PCA


class TransformImgToVec:
    __pattern = '[(.png)(.jpg)(.jpeg)]$'

    def __init__(self, images: Optional[list]=None, path_dir: Optional[str] = None):
        """

        :param images:
        :param path_dir:
        """
        self.model_name = 'clip-ViT-B-16'
        self.st_model = SentenceTransformer(self.model_name)
        self.path_dir = path_dir
        self.images = images
        if self.path_dir is not None and self.images is None:
            self.path_images = [self.path_dir + path_f for path_f in os.listdir(self.path_dir) if
                                re.search(self.__pattern, path_f)]
        self.dict_vec = {}
        self.vectors = None
        return

    def img2vec(self, img):
        """

        :param img:
        :return:
        """
        if isinstance(img, str):
            return self.st_model.encode(Image.open(img))
        return self.st_model.encode(img)

    def transform(self):
        """

        :return:
        """
        if self.images is not None:
            raise ValueError("The list with images is not empty.")
        for path_image in self.path_images:
            try:
                image = Image.open(path_image)
            except PIL.UnidentifiedImageError:
                continue
            vec = self.img2vec(image)
            os_name = os.path.basename(path_image)
            self.dict_vec[os_name] = vec
        self.vectors = list(self.dict_vec.values())
        return

    def write_file(self, name_out_file: str):
        df = pd.DataFrame(self.dict_vec.items(), columns=['Image', 'Embedding'])
        df.to_json('result.json')
        return

    def read_file(self, name_out_file: str):
        data_df = pd.read_json('result.json')
        data_df['Embedding'] = data_df['Embedding'].apply(lambda x: np.array(x))
        self.dict_vec = data_df.to_dict('records')
        self.vectors = [i['Embedding'] for i in self.dict_vec]
        return


if __name__ == '__main__':
    tr = TransformImgToVec(path_dir=r'D:\projects\parsingTinder\images1\\')
    # tr.transform()
    tr.read_file('out.csv')
    print([i['Image'] for i in tr.dict_vec])
    X = tr.vectors
    pca = PCA(n_components=2)
    X_reduced = pca.fit_transform(X)
    print(X_reduced)
    y = [1 for i in X_reduced]
    plt.scatter(X_reduced[:, 0], X_reduced[:, 1], c=y, cmap='viridis')
    plt.title("Визуализация данных Iris с PCA")
    plt.xlabel("Главная компонента 1")
    plt.ylabel("Главная компонента 2")
    plt.show()
